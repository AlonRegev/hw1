#include "linkedList.h"

/* function adds element to the start of the list.
head: double ptr to first node, newValue: value of new node.*/
void add(node** head, int newValue) {
	node* temp = *head;
	*head = createNode(newValue);
	(*head)->next = temp;
}

/* function removes first element of the list.
head: double ptr to first node.
return: removed value*/
int remove(node** head) {
	int value = -1;
	node* temp = *head;
	if (*head) {
		value = (*head)->value;
		*head = (*head)->next;
		delete(temp);
	}
	return value;
}

/* function clears memory of list.
head: ptr to first node.*/
void deleteList(node* head) {
	if (head) {
		if (head->next) {
			deleteList(head->next);
		}
		delete head;
	}
}

/* function creates a new node.
value: new node's value
return: ptr to new node. */
node* createNode(int value) {
	node* newNode = new node;
	newNode->next = 0;
	newNode->value = value;

	return newNode;
}
