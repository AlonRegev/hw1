#include "stack.h"

void push(stack* s, unsigned int element) {
	add(&s->elements, element);
}
int pop(stack* s) {
	return remove(&s->elements);
}

void initStack(stack* s) {
	s->elements = 0;	// empty list
}
void cleanStack(stack* s) {
	deleteList(s->elements);
	s->elements = 0;
}