#pragma once

typedef struct node {
	int value;
	node* next;
}node;

void add(node** head, int newValue); 
int remove(node** head);
void deleteList(node* head);
node* createNode(int value);