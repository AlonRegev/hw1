#include "utils.h"

void reverse(int* nums, unsigned int size) {
	int i = 0;
	stack* temp = new stack;
	initStack(temp);

	// add nums to stack
	for (i = 0; i < size; i++) {
		push(temp, nums[i]);
	}
	// move stack elements back to nums
	for (i = 0; i < size; i++) {
		nums[i] = pop(temp);
	}

	cleanStack(temp);
	delete(temp);
}

int* reverse10() {
	int i = 0;
	int* arr = new int[LEN];

	for (i = 0; i < LEN; i++) {
		std::cout << "Enter num " << i << ": ";
		std::cin >> arr[i];
	}

	reverse(arr, LEN);

	return arr;
}