#include "queue.h"

/* function inits queue.
q: queue ptr, size: size of queue. */
void initQueue(queue* q, unsigned int size) {
	q->elements = new int[size];
	q->size = size;
	q->count = 0;
	q->start = 0;
}

/* function clears queue's memory.
q: queue ptr*/
void cleanQueue(queue* q) {
	delete[] q->elements;
	q->size = 0;
	q->count = 0;
	q->start = 0;
}

/* function adds an element to the end of a queue.
q: queue ptr, newValue: value to add to the end of the queue. */
void enqueue(queue* q, unsigned int newValue) {
	if (q->count < q->size) {
		q->elements[(q->start + q->count) % q->size] = newValue;
		q->count++;
	}
}
/* function removes first element of a queue.
q: queue ptr. */
int dequeue(queue* q) {
	int firstValue = -1, i = 0;
	if (q->count > 0) {
		firstValue = q->elements[q->start];
		// remove first value and moves start
		q->elements[q->start] = 0;
		q->start = (q->start + 1) % q->size;
		q->count--;
	}
	return firstValue;
}