#ifndef QUEUE_H
#define QUEUE_H


/* a queue contains positive integer values. */
typedef struct queue
{
	int* elements;
	int size;
	int count;
	int start;
} queue;
/* bonus explanation:
added start: index of start of queue.
every time an element is removed, start moves forward.
the indexes go back to zero if they are bigger than the size.
index i -> (start + i) % size */

void initQueue(queue* q, unsigned int size);
void cleanQueue(queue* q);

void enqueue(queue* q, unsigned int newValue);
int dequeue(queue* q); // return element in top of queue, or -1 if empty

#endif