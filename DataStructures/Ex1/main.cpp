#include <iostream>
#include "queue.h"

int main() {
	queue* temp = new queue;
	initQueue(temp, 4);

	enqueue(temp, 1);
	enqueue(temp, 2);
	enqueue(temp, 3);
	enqueue(temp, 4);

	std::cout << "first:  " << dequeue(temp) << "\n";
	std::cout << "second: " << dequeue(temp) << "\n\n"; 

	enqueue(temp, 1);
	enqueue(temp, 2);

	std::cout << "first:  " << dequeue(temp) << "\n";
	std::cout << "second: " << dequeue(temp) << "\n";
	std::cout << "third:  " << dequeue(temp) << "\n"; 
	std::cout << "fourth: " << dequeue(temp) << "\n";
	std::cout << "fifth:  " << dequeue(temp) << "\n";

	delete(temp);
}
